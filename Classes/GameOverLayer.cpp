#define SCALE_RATIO 200
#include "GameOverLayer.h"
#include "MenuLayer.h"

#include <iostream>

using namespace cocos2d;

//Constants
const int playerCollisionBitmask = 1;
const int projectileCollisionBitmask = 2;


const float playerMovementSpeed = 3.0f;
const int targetObjectHealth = 10;

GameOverLayer::GameOverLayer() {

}


bool GameOverLayer::init() {
	// call to super
	if (!Layer::init())
	{
		return false;
	}

	log("game over init called");

	_screenSize = Director::getInstance()->getWinSize();
	_center = Vec2(_screenSize.width * 0.5, _screenSize.height * 0.5);

	auto gameOverScreen = Sprite::create();
	gameOverScreen->initWithFile("res/GameOver.png");
	gameOverScreen->setPosition(_center);
	this->addChild(gameOverScreen);
	
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(GameOverLayer::onKeyPressed, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
	
	//create main loop
	this->scheduleUpdate();
	return true;

}

Scene * GameOverLayer::scene()
{
	auto scene = Scene::create();;
	auto layer = GameOverLayer::create();
		
	scene->addChild(layer);

	return scene;
}



void GameOverLayer::update(float dt) {

}

void GameOverLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event) {
	Director::getInstance()->replaceScene(MenuLayer::scene());
}




GameOverLayer::~GameOverLayer()
{
}

