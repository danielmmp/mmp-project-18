#pragma once
#ifndef __GAMEOVERLAYER_H__
#define __GAMEOVERLAYER_H__


#include "cocos2d.h"

class GameOverLayer : public cocos2d::Layer

{
	//Scene items
	cocos2d::Size _screenSize;
	cocos2d::Vec2 _center;

	
public:
	GameOverLayer();
	virtual ~GameOverLayer();
	virtual bool init();


	static cocos2d::Scene* scene();

	CREATE_FUNC(GameOverLayer);

	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event);


private:
	void update(float dt);
};


#endif //__GAMEOVERLAYER_H__

