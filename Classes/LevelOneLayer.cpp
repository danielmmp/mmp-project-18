#define SCALE_RATIO 200
#include "LevelOneLayer.h"
#include "GameOverLayer.h"
#include "MenuLayer.h"

#include <iostream>

using namespace cocos2d;

//Constants
const int playerCollisionBitmask = 1;
const int projectileCollisionBitmask = 2;


const float playerMovementSpeed = 3.0f;
const int targetObjectHealth = 10;

LevelOneLayer::LevelOneLayer() {

}


bool LevelOneLayer::init() {
	// call to super
	if (!Layer::init())
	{
		return false;
	}

	log("level one init called");

	_screenSize = Director::getInstance()->getWinSize();
	_center = Vec2(_screenSize.width * 0.5, _screenSize.height * 0.5);

	auto levelOneBg = Sprite::create();
	levelOneBg->initWithFile("res/LevelOne.png");
	levelOneBg->setPosition(_center);
	this->addChild(levelOneBg);

	timeRemainingLabel = Label::createWithSystemFont(std::to_string(time), "Arial", 12.0f);
	//timeRemainingLabel->setString(std::to_string(time));
	timeRemainingLabel->setPosition(_center);
	timeRemainingLabel->setTextColor(Color4B::WHITE);
	this->addChild(timeRemainingLabel);

	//Ground
	groundTerrain = Sprite::create("res/TerrainTest.png", Rect(0.0f, 0.0f, 100.0f, 50.0f));
	//groundTerrain->initWithFile("res/TerrainTest.png");
	groundTerrain->setPosition(Vec2(_screenSize.width * 0.5, 0.0f));
	auto groundBody = PhysicsBody::createBox(
		Size(100.0f, 10.0f),//Size(_screenSize.width, 10.0f),
		PhysicsMaterial(0.1f, 1.0f, 0.5f)
	);
	groundBody->setDynamic(false);
	groundTerrain->setPhysicsBody(groundBody);
	this->addChild(groundTerrain);


	//Player object
	playerSprite = Sprite::create();
	playerSprite->initWithFile("res/court.png", Rect(0.0f, 0.0f, 50.0f, 100.0f));
	playerSprite->setPosition(Vec2(_screenSize.width * 0.5, 10.0f));

	auto playerBody = PhysicsBody::createBox(
		playerSprite->getContentSize(),
		PhysicsMaterial(0.1f, 0.1f, 0.5f)
	);
	playerBody->setCollisionBitmask(playerCollisionBitmask);
	playerSprite->setPhysicsBody(playerBody);
	this->addChild(playerSprite);

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(LevelOneLayer::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(LevelOneLayer::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	auto collisionListener = EventListenerPhysicsContact::create();
	collisionListener->onContactBegin = CC_CALLBACK_1(LevelOneLayer::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(collisionListener, this);

	//create main loop
	this->scheduleUpdate();
	return true;

}

Scene * LevelOneLayer::scene()
{
	auto scene = Scene::createWithPhysics();;
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	scene->getPhysicsWorld()->setGravity(Vec2(0.0f, -500.0f));
	auto layer = LevelOneLayer::create();
		
	scene->addChild(layer);

	return scene;
}



void LevelOneLayer::update(float dt) {

	//update timer
	time -= dt;
	if (time <= 0) { time = 0;  
	Director::getInstance()->replaceScene(GameOverLayer::scene());
	}
	this->timeRemainingLabel->setString("Zeit verbleibend: " + std::to_string((int) time));

	if (movingLeft || movingRight) { move(); }
	if (chargingShot) { chargeShot(); }
	if (aimingUpward || aimingDownward) { updateAim(); }


}


void LevelOneLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event * event)
{
		switch (keyCode) {
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			log("right arrow pressed");
			movingRight = movingLeft ? false : true;
			aimDirectionX = 100.0f;
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			log("left arrow pressed");
			movingLeft = movingRight ? false : true;
			aimDirectionX = -100.0f;
			break;
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			log("up arrow pressed");
			aimingUpward = aimingDownward ? false : true;
			break;	
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			log("down arrow pressed");
			aimingDownward = aimingUpward ? false : true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_CTRL:
			log("right ctrl pressed");
			chargingShot = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_SHIFT:
			log("right shift pressed");
			break;
		}
}


void LevelOneLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event * event)
{
	switch (keyCode) {
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		log("right arrow pressed");
		movingRight = false;
		break;
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		log("left arrow pressed");
		movingLeft = false;
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
		log("up arrow pressed");
		aimingUpward = false;
		break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
		log("down arrow pressed");
		aimingDownward = false;
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_CTRL:
		log("right ctrl pressed");
		shoot();
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_SHIFT:
		log("right shift pressed");
		break;
	case EventKeyboard::KeyCode::KEY_ESCAPE:
		log("escape pressed");
		Director::getInstance()->replaceScene(MenuLayer::scene());
		break;
	}
}

void LevelOneLayer::shoot()
{
	auto projectile = Sprite::create();
	auto projectileBody = PhysicsBody::createCircle(
		10.0f,
		PhysicsMaterial(0.01f, 0.1f, 0.5f)
	);
	projectileBody->setCollisionBitmask(projectileCollisionBitmask);
	projectile->initWithFile("res/puck.png");
	projectile->setPosition(playerSprite->getPosition());
	this->addChild(projectile);
	projectile->setPhysicsBody(projectileBody);
	projectile->getPhysicsBody()
		->applyImpulse(Vec2(aimDirectionX * shotChargeMultiplier, aimDirectionY * shotChargeMultiplier));
	//projectile->getPhysicsBody()->applyImpulse(Vec2(1000.0f, 1000.0f));
	resetAim();
}

void LevelOneLayer::move()
{
	auto playerPos = playerSprite->getPosition();
	auto playerSize = playerSprite->getContentSize();
	if (movingRight) {
		playerPos.x += playerMovementSpeed;
		if (playerPos.x > _screenSize.width - playerSize.width * 0.5f) {
			playerPos.x -= playerMovementSpeed;
		}
	}
	if (movingLeft) {
		playerPos.x -= playerMovementSpeed;
		if (playerPos.x < playerSize.width * 0.5f) {
			playerPos.x += playerMovementSpeed;
		}
	}
	playerSprite->setPosition(playerPos);
}

void LevelOneLayer::chargeShot()
{
	shotChargeMultiplier += 0.1f;
}

void LevelOneLayer::updateAim()
{
	if (aimingUpward) {
		aimDirectionY += (aimDirectionY >= 1000.0) ? 0.0f : 5.0f;
	}
	else if (aimingDownward) {
		aimDirectionY -= (aimDirectionY <= 0.0) ? 0.0f : 0.5f;
	}
}



void LevelOneLayer::resetAim()
{
	aimDirectionX = 100.0f;
	aimDirectionY = 100.0f;
	shotChargeMultiplier = 1.0f;
	chargingShot = false;
}

bool LevelOneLayer::onContactBegin(PhysicsContact &contact) {
	log("contact began");
	return true;
}

LevelOneLayer::~LevelOneLayer()
{
}

