#pragma once
#ifndef __LEVELONELAYER_H__
#define __LEVELONELAYER_H__


#include "cocos2d.h"

class LevelOneLayer : public cocos2d::Layer

{
	//Scene items
	cocos2d::Size _screenSize;
	cocos2d::Vec2 _center;

	cocos2d::Sprite* groundTerrain;
	cocos2d::Sprite* playerSprite;

	cocos2d::Label* timeRemainingLabel;

	//Game timer
	float time = 5.0f;


	//Player character values
	bool movingLeft = false;
	bool movingRight = false;
	bool chargingShot = false;
	bool aimingUpward = false;
	bool aimingDownward = false;
	
	float aimDirectionX = 100.0f;
	float aimDirectionY = 50.0f;
	float shotChargeMultiplier = 1.0f;

public:
	LevelOneLayer();
	virtual ~LevelOneLayer();
	virtual bool init();


	static cocos2d::Scene* scene();

	CREATE_FUNC(LevelOneLayer);

	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event * event);

	void shoot();
	void move();
	void chargeShot();
	void updateAim();
	void resetAim();

	bool onContactBegin(cocos2d::PhysicsContact &contact);

private:
	void update(float dt);
};


#endif //__LEVELONEAYER_H__

