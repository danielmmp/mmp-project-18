#define SCALE_RATIO 200
#include "MenuLayer.h"
#include "LevelOneLayer.h"

#include <iostream>

using namespace cocos2d;

MenuLayer::MenuLayer() {

}


bool MenuLayer::init() {
	// call to super
	if (!Layer::init())
	{
		return false;
	}
	
	log("init called");

	_screenSize = Director::getInstance()->getWinSize();
	_center = Vec2(_screenSize.width * 0.5, _screenSize.height * 0.5);

	auto menuBackground = Sprite::create();
	menuBackground->initWithFile("res/Title.png");
	menuBackground->setPosition(_center);
	this->addChild(menuBackground);

	auto playItem = MenuItemImage::create("res/PlayNormal.png", "res/PlaySelected.png",
		[&](Ref* sender) {
		log("Play Clicked");
	});

	auto levelOneItem = MenuItemImage::create("res/L1Normal.png", "res/L1Selected.png",
		[&](Ref* sender) {
		log("Level One Clicked");
		auto levelOneScene = LevelOneLayer::scene();
		Director::getInstance()->replaceScene(levelOneScene);
	});

	auto *menu = Menu::create(playItem, levelOneItem, NULL);
	menu->setPosition(_center);
	menu->alignItemsHorizontallyWithPadding(50.0f);
	this->addChild(menu);

	
	//create main loop
	this->scheduleUpdate();
	return true;

}

Scene * MenuLayer::scene()
{
	auto scene = Scene::createWithPhysics();;
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	scene->getPhysicsWorld()->setGravity(Vec2(0.0f, -350.0f));
	auto layer = MenuLayer::create();

	scene->addChild(layer);

	return scene;
}



void MenuLayer::update(float dt) {


}

MenuLayer::~MenuLayer()
{
}

