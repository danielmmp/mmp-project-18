#pragma once
#ifndef __MENULAYER_H__
#define __MENULAYER_H__


#include "cocos2d.h"

class MenuLayer : public cocos2d::Layer

{
	cocos2d::Size _screenSize;
	cocos2d::Vec2 _center;


public:
	MenuLayer();
	virtual ~MenuLayer();
	virtual bool init();

	static cocos2d::Scene* scene();

	CREATE_FUNC(MenuLayer);

private:
	void update(float dt);
};


#endif //__MENULAYER_H__

